/*
 * font.h
 *
 *  Created on: Mar 15, 2014
 *      Author: koizumi
 */

#ifndef FONT_H_
#define FONT_H_

#include <stdint.h>

#define SUPPORTED_ASCII_CHARS    161

extern const uint8_t asciiFontTable[];

#endif /* FONT_H_ */
