/* @file disp_hal.h
 * @brief Hardware Abstraction Layer of display driver.
 *
 * The functions declared in this file should be implemented on your platform.
 *
 *  Created on: Mar 15, 2014
 *      Author: koizumi
 */

#ifndef DISP_HAL_H_
#define DISP_HAL_H_

#include <stdint.h>

/** @brief Configure the pripheral.
 *
 * Configure peripherals required for TD-T14ST2G2360-5.
 * For example, GPIO setting, SPI setting, etc.
 *
 * @return Void.
 */
void DISP_HAL_configurePeripheral( void);

/** @brief Wait @p t micro-second.
 *
 * Wait @p t micro-second.
 *
 * @return Void.
 */
void DISP_HAL_waitMicroSecond( uint32_t t);

/** @brief Toggle the pin connected to AO to high level.
 *
 * @return Void.
 */
void DISP_HAL_toggleCommandTransfer( void);

/** @brief Toggle the GPIO pin connected to AO to low level.
 *
 * @return Void.
 */
void DISP_HAL_toggleDataTransfer( void);

/** @brief Set the GPIO pin connected to RESET_N to low level.
 *
 * @return Void.
 */
void DISP_HAL_enableReset( void);

/** @brief Set the GPIO pin connected to RESET_N to high level.
 *
 * @return Void.
 */
void DISP_HAL_disableReset( void);

/** @brief Send data.
 *
 * Send data in @p buf to the device.
 *
 * @param buf The data to be sent.
 * @param nBytes The size of the data to be sent.
 * @return Void.
 */
void DISP_HAL_sendBytes( const uint8_t *buf, uint16_t nBytes);

#endif /* DISP_HAL_H_ */
