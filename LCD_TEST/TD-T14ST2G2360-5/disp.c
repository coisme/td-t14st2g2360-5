/** @file disp.c
 *  @brief Display driver for TD-T14ST2G2360-5.
 *
 *  @author Osamu Koizumi
 */

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "disp.h"
#include "disp_hal.h"

#ifndef DISP_EXCLUDE_FONTS
#include "font.h"
#endif

#define DISP_WIDTH     128     // pixel
#define DISP_HEIGHT    128     // pixel

#define DISP_CHAR_WIDTH      8    // pixel
#define DISP_CHAR_HEIGHT     16   // pixel

#define DISP_MAX_COL      (DISP_WIDTH/DISP_CHAR_WIDTH)
#define DISP_MAX_ROW      (DISP_HEIGHT/DISP_CHAR_HEIGHT)

#define DISP_WAIT_RESET_PULSE         100      // Minimum reset pulse width is 50 us
#define DISP_WAIT_RESET               150000   // Maximum reset completion time is 120 ms
#define DISP_WAIT_SLEEP_OUT           150000   // Requred wait time is 100 ms

// Command definitions
#define DISP_CMD_SLEEP_OUT                0x11
#define DISP_CMD_GAMMA                    0x26
#define DISP_CMD_FRAME_RATE               0xB1
#define DISP_CMD_PWR_CTRL_1               0xC0
#define DISP_CMD_PWR_CTRL_2               0xC1
#define DISP_CMD_VCOM_CTRL_1              0xC5
#define DISP_CMD_INTERFACE_PIXEL_FORMAT   0x3A
#define DISP_CMD_MEMORY_ACCESS_CTRL       0x36
#define DISP_CMD_COLUMN_ADDR_SET          0x2A
#define DISP_CMD_PAGE_ADDR_SET            0x2B
#define DISP_CMD_INVERSION_CTRL           0xB4
#define DISP_CMD_GAM_R_SEL                0xF2
#define DISP_CMD_POSITIVE_GAMMA_CORR      0xE0
#define DISP_CMD_NEGATIVE_GAMMA_CORR      0xE1
#define DISP_CMD_ON                       0x29
#define DISP_CMD_OFF                      0x28
#define DISP_CMD_MEMORY_WRITE             0x2C

// Default Values
static const uint8_t DISP_VAL_DEFAULT_GAMMA = 0x04;
static const uint8_t DISP_VAL_DEFAULT_FRAME_RATE[] = { 0x0E, 0x10};
static const uint8_t DISP_VAL_DEFAULT_PWR_CTRL_1[] = { 0x08, 0x00};
static const uint8_t DISP_VAL_DEFAULT_PWR_CTRL_2 = 0x05;
static const uint8_t DISP_VAL_DEFAULT_VCOM_CTRL_1[] = { 0x38, 0x40};
static const uint8_t DISP_VAL_DEFAULT_IFACE_PIXEL_FMT = 0x06; // 6-6-6 bits
static const uint8_t DISP_VAL_DEFAULT_MEMORY_ACCESS_CTRL = 0xC8;
static const uint8_t DISP_VAL_DEFAULT_COLUMN_ADDR_SET[] = { 0x00, 0x00, 0x00, 0x7F};
static const uint8_t DISP_VAL_DEFAULT_PAGE_ADDR_SET[] = { 0x00, 0x00, 0x00, 0x7F};
static const uint8_t DISP_VAL_DEFAULT_INVERSION_CTRL = 0x00;
static const uint8_t DISP_VAL_DEFAULT_GAM_R_SEL = 0x01;
static const uint8_t DISP_VAL_DEFAULT_POSITIVE_GAMMA_CORR[] = {
		0x3F, 0x22, 0x20, 0x30, 0x29, 0x0C, 0x4E, 0xB7,
		0x3C, 0x19, 0x22, 0x1E, 0x02, 0x01, 0x00
	};
static const uint8_t DISP_VAL_DEFAULT_NEGATIVE_GAMMA_CORR[] = {
		0x00, 0x1B, 0x1F, 0x0F, 0x16, 0x13, 0x31, 0x84,
		0x43, 0x06, 0x1D, 0x21, 0x3D, 0x3E, 0x3F
};

// Brightness
#define BR_LIGHT 0xff
#define BR_DARK  0x80
#define BR_NONE  0x00
const Color DISP_COLOR_WHITE = { BR_LIGHT, BR_LIGHT, BR_LIGHT};
const Color DISP_COLOR_LIGHT_RED = { BR_LIGHT, BR_NONE, BR_NONE};
const Color DISP_COLOR_DARK_RED = { BR_DARK, BR_NONE, BR_NONE};
const Color DISP_COLOR_LIGHT_GREEN = { BR_NONE, BR_LIGHT, BR_NONE};
const Color DISP_COLOR_DARK_GREEN = { BR_NONE, BR_DARK, BR_NONE};
const Color DISP_COLOR_LIGHT_BLUE = { BR_NONE, BR_NONE, BR_LIGHT};
const Color DISP_COLOR_DARK_BLUE = { BR_NONE, BR_NONE, BR_DARK};

// maintain plotting color
#define DISP_IDX_RED     0
#define DISP_IDX_GREEN   1
#define DISP_IDX_BLUE    2
static uint8_t foregroundColor[3];
static uint8_t backgroundColor[3];

/*
 * Private functions
 */
static void sendCommand( uint8_t byte){
	DISP_HAL_toggleCommandTransfer();
	DISP_HAL_sendBytes( &byte, 1);
}

static void sendData( const uint8_t *buf, uint16_t n){
	DISP_HAL_toggleDataTransfer();
	DISP_HAL_sendBytes( buf, n);
}

static void sendPacket( uint8_t cmd, const uint8_t *data, uint16_t dataLength){
	sendCommand( cmd);
	sendData( data, dataLength);
}

static void outputPixel( uint8_t x, uint8_t y)
{
	uint8_t xx[] = { 0, x, 0, x};
	uint8_t yy[] = { 0, y, 0, y};

	if( x > DISP_WIDTH || y > DISP_HEIGHT) // out of range
		return;

	sendPacket( DISP_CMD_COLUMN_ADDR_SET, xx, sizeof(xx));
	sendPacket( DISP_CMD_PAGE_ADDR_SET, yy, sizeof(yy));
	sendPacket( DISP_CMD_MEMORY_WRITE, foregroundColor, sizeof(foregroundColor));
}

/*
 * Public functions
 */
#ifndef DISP_EXCLUDE_FONTS
void DISP_drawCharacterAt( uint8_t x0, uint8_t y0, uint8_t c){
	uint8_t i, j;
	const uint8_t *pt;

	// a pointer to the character's 8x16 pixels font
	pt = &asciiFontTable[((uint16_t)(c)*16)];

	// plot the character
	for( j=0; j<16; j++){  // forward to the y-direction
      	for( i=0; i<8; i++){  // forward to the x-direction
		  	if(*(pt+j)&(0x80 >> i)){  // the pixel should be plotted
		    	outputPixel( x0 + i, y0 + j);
		  	}
	   	}
    }
}

void DISP_drawStringAt( uint8_t x0, uint8_t y0, const uint8_t *s, uint8_t nBytes)
{
	uint8_t i;
	uint8_t c;

	for( i=0; i < nBytes; i++){
		c = (uint8_t)(*(s+i));
		if( c >= SUPPORTED_ASCII_CHARS || c == '\0'){ // unsupported characters or end of the string
			break;
		}
		else{
			DISP_drawCharacterAt( x0 + i*DISP_CHAR_WIDTH, y0, *(s+i));
		}
	}
	return;
}

void DISP_drawStringGridAt( uint8_t col, uint8_t row, const uint8_t *s, uint8_t nBytes, bool wrap){
	uint8_t i = 0;
	uint8_t nc;         // column counts
	uint8_t npx, npy;   // coordinates in pixels

	npx = col * DISP_CHAR_WIDTH;
	npy = row * DISP_CHAR_HEIGHT;

	nc = DISP_MAX_COL - col;

	while( i < nBytes){
		DISP_drawStringAt( npx, npy, s, nc);
		if( ! wrap)
			break;
		s += nc;
		i += nc;
		nc = DISP_MAX_COL;
		npx = 0;
		npy += DISP_CHAR_HEIGHT;
	}
}
#endif

void DISP_drawLine( uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2){
	// Bresenham's algorithm
	// http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
	int8_t x, y;
	int8_t sx, sy;
	int8_t dx, dy;
	int16_t err,e2;

	dx = abs( x2 - x1);
	dy = abs( y2 - y1);

	sx = (x1 < x2) ? 1 : -1;
	sy = (y1 < y2) ? 1 : -1;

	err = dx - dy;

	x = x1;
	y = y1;

	while( true){
		outputPixel( x, y);
		if( x == x2 && y == y2)
			break;
		e2 = 2*err;
		if( e2 > -dy){
			err = err - dy;
			x += sx;
		}
		if( e2 < dx){
			err = err + dx;
			y += sy;
		}
	}
}

void DISP_drawRect( uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2){
	DISP_drawLine( x1, y1, x1, y2);
	DISP_drawLine( x1, y2, x2, y2);
	DISP_drawLine( x2, y2, x2, y1);
	DISP_drawLine( x2, y1, x1, y1);
}



void DISP_reset( void){
	DISP_HAL_enableReset();
	DISP_HAL_waitMicroSecond( DISP_WAIT_RESET_PULSE);
	DISP_HAL_disableReset();
	DISP_HAL_waitMicroSecond( DISP_WAIT_RESET);
}

void DISP_init( void){
	DISP_HAL_configurePeripheral();

	DISP_reset();

	sendCommand( DISP_CMD_SLEEP_OUT);
	DISP_HAL_waitMicroSecond( DISP_WAIT_SLEEP_OUT);

	sendPacket( DISP_CMD_GAMMA, &DISP_VAL_DEFAULT_GAMMA, sizeof( DISP_VAL_DEFAULT_GAMMA));
	sendPacket( DISP_CMD_FRAME_RATE, DISP_VAL_DEFAULT_FRAME_RATE, sizeof( DISP_VAL_DEFAULT_FRAME_RATE));
	sendPacket( DISP_CMD_PWR_CTRL_1, DISP_VAL_DEFAULT_PWR_CTRL_1, sizeof( DISP_VAL_DEFAULT_PWR_CTRL_1));
	sendPacket( DISP_CMD_PWR_CTRL_2, &DISP_VAL_DEFAULT_PWR_CTRL_2, sizeof( DISP_VAL_DEFAULT_PWR_CTRL_2));
	sendPacket( DISP_CMD_VCOM_CTRL_1, DISP_VAL_DEFAULT_VCOM_CTRL_1, sizeof( DISP_VAL_DEFAULT_VCOM_CTRL_1));
	sendPacket( DISP_CMD_INTERFACE_PIXEL_FORMAT, &DISP_VAL_DEFAULT_IFACE_PIXEL_FMT, sizeof(DISP_VAL_DEFAULT_IFACE_PIXEL_FMT));
	sendPacket( DISP_CMD_MEMORY_ACCESS_CTRL, &DISP_VAL_DEFAULT_MEMORY_ACCESS_CTRL, sizeof(DISP_VAL_DEFAULT_MEMORY_ACCESS_CTRL));
	sendPacket( DISP_CMD_COLUMN_ADDR_SET, DISP_VAL_DEFAULT_COLUMN_ADDR_SET, sizeof(DISP_VAL_DEFAULT_COLUMN_ADDR_SET));
	sendPacket( DISP_CMD_PAGE_ADDR_SET, DISP_VAL_DEFAULT_PAGE_ADDR_SET, sizeof(DISP_VAL_DEFAULT_PAGE_ADDR_SET));
	sendPacket( DISP_CMD_INVERSION_CTRL, &DISP_VAL_DEFAULT_INVERSION_CTRL, sizeof(DISP_VAL_DEFAULT_INVERSION_CTRL));
	sendPacket( DISP_CMD_GAM_R_SEL, &DISP_VAL_DEFAULT_GAM_R_SEL, sizeof(DISP_VAL_DEFAULT_GAM_R_SEL));
	sendPacket( DISP_CMD_POSITIVE_GAMMA_CORR, DISP_VAL_DEFAULT_POSITIVE_GAMMA_CORR, sizeof(DISP_VAL_DEFAULT_POSITIVE_GAMMA_CORR));
	sendPacket( DISP_CMD_NEGATIVE_GAMMA_CORR, DISP_VAL_DEFAULT_NEGATIVE_GAMMA_CORR, sizeof(DISP_VAL_DEFAULT_NEGATIVE_GAMMA_CORR));
	sendCommand( DISP_CMD_ON);
}

void DISP_setForegroundColor( const Color c){
	foregroundColor[DISP_IDX_RED] = (c.r >> 2);
	foregroundColor[DISP_IDX_GREEN] = (c.g >> 2);
	foregroundColor[DISP_IDX_BLUE] = (c.b >> 2);
}

void DISP_setForegroundColorRGB( uint8_t r, uint8_t g, uint8_t b){
	Color c = { r, g, b};
	DISP_setForegroundColor( c);
}

void DISP_setBackgroundColor( const Color c){
	backgroundColor[DISP_IDX_RED] = (c.r >> 2);
	backgroundColor[DISP_IDX_GREEN] = (c.g >> 2);
	backgroundColor[DISP_IDX_BLUE] = (c.b >> 2);
}

void DISP_setBackgroundColorRGB( uint8_t r, uint8_t g, uint8_t b){
	Color c = { r, g, b};
	DISP_setBackgroundColor( c);
}

void DISP_clear(){
	uint8_t i, j;

	sendPacket( DISP_CMD_COLUMN_ADDR_SET, DISP_VAL_DEFAULT_COLUMN_ADDR_SET, sizeof(DISP_VAL_DEFAULT_COLUMN_ADDR_SET));
	sendPacket( DISP_CMD_PAGE_ADDR_SET, DISP_VAL_DEFAULT_PAGE_ADDR_SET, sizeof(DISP_VAL_DEFAULT_PAGE_ADDR_SET));
	sendCommand( DISP_CMD_MEMORY_WRITE);
	for( i=0; i < DISP_HEIGHT; i++){
		for( j=0; j < DISP_WIDTH; j++){
			sendData( backgroundColor, sizeof(backgroundColor));
		}
	}

}
