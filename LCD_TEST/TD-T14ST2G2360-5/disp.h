/* @file disp.h
 * @brief Function prototypes for TD-T14ST2G2360-5 display driver.
 *
 * @author Osamu Koizumi
 */

#ifndef DISP_H_
#define DISP_H_

#include <stdbool.h>
#include <stdint.h>

// If you don't plot ASCII characters on the display,
// uncomment the #define below. Program size will be saved.
//
//#define DISP_EXCLUDE_FONTS

typedef struct _Color{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} Color;

extern const Color DISP_COLOR_WHITE;
extern const Color DISP_COLOR_LIGHT_RED;
extern const Color DISP_COLOR_DARK_RED;
extern const Color DISP_COLOR_LIGHT_GREEN;
extern const Color DISP_COLOR_DARK_GREEN;
extern const Color DISP_COLOR_LIGHT_BLUE;
extern const Color DISP_COLOR_DARK_BLUE;

/** @brief Reset the device.
 *
 * @return Void.
 */
void DISP_reset( void);

/** @brief Initialize the device.
 *
 * @return Void.
 */
void DISP_init( void);

/** @brief Set plotting color.
 *
 *  @param[in] c color of plot
 *  @return Void.
 */
void DISP_setForegroundColor( const Color c);

/** @brief Set plotting color.
 *
 * @param[in] r Brightness of red color.
 * @param[in] g Brightness of green color.
 * @param[in] b Brightness of blue color.
 * @return Void.
 */
void DISP_setForegroundColorRGB( uint8_t r, uint8_t g, uint8_t b);

/** @brief Set background color.
 *
 *  @param[in] c color of background
 *  @return Void.
 */
void DISP_setBackgroundColor( const Color c);

/** @brief Set background color.
 *
 * @param[in] r Brightness of red color.
 * @param[in] g Brightness of green color.
 * @param[in] b Brightness of blue color.
 * @return Void.
 */
void DISP_setBackgroundColorRGB( uint8_t r, uint8_t g, uint8_t b);

/** @brief Clear the display.
 *
 * Clear the display with the background color.
 *
 * @return Void.
 */
void DISP_clear( void);

/** @brief Draw a line.
 *
 * Draw a finite line from ( @p x1 , @p y1 ) to ( @p x2 , @p y2 ) with the foreground color.
 *
 * @param[in] x1 The x coordinate of the start point of the line.
 * @param[in] y1 The y coordinate of the start point of the line.
 * @param[in] x2 The x coordinate of the end point of the line.
 * @param[in] y2 The y coordinate of the end point of the line.
 * @return Void.
 */
void DISP_drawLine( uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);


/** @brief Draw a rectangle
 *
 *  Draw a rectangle whose the top left corner is ( @p x1 , @p y1 ) and the bottom right corner
 *  is ( @p x2, @p y2 ).
 *
 *  @param[in] x1 The x coordinate of the top left corner of the rectangle.
 *  @param[in] y1 The y coordinate of the top left corner of the rectangle.
 *  @param[in] x2 The x coordinate of the bottom right corner of the rectangle.
 *  @param[in] y2 The y coordinate of the bottom right corner of the rectangle.
 *  @return Void.
 */
void DISP_drawRect( uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);

#ifndef DISP_EXCLUDE_FONTS
/** @brief Draw a character.
 *
 * Draw the character at the point specified by the arguments with the foreground color.
 *
 * @param[in] x0 The x coordinate of upper left corner of the character to be drawn.
 * @param[in] y0 The y coordinate of upper left corner of the character to be drawn.
 * @param[in] c The character to be drawn.
 * @return Void.
 */
void DISP_drawCharacterAt( uint8_t x0, uint8_t y0, uint8_t c);

/** @brief Draw a string.
 *
 *  Draw the string at the point specified by the arguments with the foreground color.
 *  If all or a part of the string go beyond the drawable area, the over characters
 *  will be ignored.
 *
 *  @param[in] x0 The x coordinate of upper left corner of the string to be drawn.
 *  @param[in] y0 The y coordinate of upper left corner of the string to be drawn.
 *  @param[in] s The string to be drawn.
 *  @param[in] nBytes The number of characters of the string.
 */
void DISP_drawStringAt( uint8_t x0, uint8_t y0, const uint8_t *s, uint8_t nBytes);

/** @brief Draw a string.
 *
 * Draw the string at the specified column and row with the foreground color.
 * If @p wrap is true, the string automatically starts a new line.
 * If @p wrap is false, all or a part of the string that be over the drawable will be
 * ignored.
 *
 * @param[in] col The column that the string begins.
 * @param[in] row The row that the string begins.
 * @param[in] s The string to be drawn.
 * @param[in] nBytes The number of characters of the string.
 * @param[in] wrap The flag which enables to start a new line automatically.
 */
void DISP_drawStringGridAt( uint8_t col, uint8_t row, const uint8_t *s, uint8_t nBytes, bool wrap);

#endif /* DISP_EXCLUDE_FONTS */

#endif /* DISP_H_ */
